import socket

configPath = "" # File path of external configuration file.
usingUDP = True # Should be set from config file. If not using UDP, the listener is using TCP.
recieveBufferSize = 1024 # Shouldn't be changed. 
listening = True # Shouldn't be changed. 
remoteIP = "" # If left as an empty string will recieve from any IP address.
localPort = 6666

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((remoteIP, localPort))

while listening: # Worried about blocking. Look in to multithreading.
	recievedMessage, addr = s.recvfrom(recieveBufferSize)
	# Recieves extra characters before and after message when sending messages from Processing.
	print ("Recieved Message: ", recievedMessage) 