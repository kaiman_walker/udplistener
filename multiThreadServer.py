'''
TCP Multithread Server

Uses a thread per client to avoid the blocking client.recv() then 
use the main thread just for listening for new clients. 
When one connects, the main thread creates a new thread that just 
listens to the new client and ends when it doesn't talk for 10 seconds.

Clients timeout after 10 seconds of inactivity and must reconnect.
'''

import socket
import threading

class ThreadedServer(object): 
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self): # Parent Thread
        self.sock.listen(5)
        print("Listening...")

        while True:
            client, address = self.sock.accept()
            client.settimeout(10) # Timeout after 10 seconds
            threading.Thread(target = self.listenToClient,args = (client,address)).start()

    def listenToClient(self, client, address): # Child Thread
        bufferSize = 1024

        while True:
            try:
                data = client.recv(bufferSize)

                if data:
                    print("Recieved Message: " + data)
                    response = data # Set the response to echo back the recieved data 
                    client.send(response)
                    print(response)
                else:
                    raise error('Client disconnected')
            except:
                client.close()
                return False

if __name__ == "__main__": # Executes the following only if this file is run directly and not if imported.
    #localPort = int(input("Port? "))
    localPort = 6666
    ThreadedServer('',localPort).listen()