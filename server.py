import threading
import socketserver

sendReply = True
debugMode = True
localPort = 8888
localHost = ""

class ThreadedUDPRequestHandler(socketserver.BaseRequestHandler):

    def handle(self):
        message = self.request[0].strip()
        socket = self.request[1]
        remoteIP = (self.client_address[0])
        remotePort = self.client_address[1]
        currentThread = threading.current_thread()

        if debugMode:
            print ("Thread: %s" % currentThread.name)
            print ("Received call from client address: %s" % remoteIP)
            print ("Received message from port: %s" % remotePort)
            print ("Message: %s" % message.decode("utf-8"))
            #print ("Active Threads: %i" % threading.active_count())
            print ("")

        if sendReply:
            socket.sendto(message, self.client_address)
        

class ThreadedUDPServer(socketserver.ThreadingMixIn, socketserver.UDPServer):
    pass

if __name__ == "__main__":
    server = ThreadedUDPServer((localHost, localPort), ThreadedUDPRequestHandler)
    ip, port = server.server_address
    server.serve_forever()
    # Start a thread with the server -- 
    # that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()
    server.shutdown()